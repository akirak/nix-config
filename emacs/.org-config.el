;; This configuration is written for org-starter:
;; <https://github.com/akirak/org-starter>

;; <https://github.com/akirak/trivial-elisps>

(org-starter-def "emacs-config.org"
  :key "e")
