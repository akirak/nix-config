{
  description = "THIS IS AN AUTO-GENERATED FILE. PLEASE DON'T EDIT IT MANUALLY.";
  inputs = {
    "a" = {
      flake = false;
      owner = "plexus";
      repo = "a.el";
      type = "github";
    };
    academic-phrases = {
      flake = false;
      owner = "nashamri";
      repo = "academic-phrases";
      type = "github";
    };
    ace-window = {
      flake = false;
      owner = "abo-abo";
      repo = "ace-window";
      type = "github";
    };
    aggressive-indent = {
      flake = false;
      owner = "Malabarba";
      repo = "aggressive-indent-mode";
      type = "github";
    };
    aio = {
      flake = false;
      owner = "skeeto";
      repo = "emacs-aio";
      type = "github";
    };
    akirak = {
      flake = false;
      type = "git";
      url = "https://gitlab.com/akirak/akirak-mode.git";
    };
    anzu = {
      flake = false;
      owner = "emacsorphanage";
      repo = "anzu";
      type = "github";
    };
    async = {
      flake = false;
      owner = "jwiegley";
      repo = "emacs-async";
      type = "github";
    };
    auto-yasnippet = {
      flake = false;
      owner = "abo-abo";
      repo = "auto-yasnippet";
      type = "github";
    };
    avy = {
      flake = false;
      owner = "abo-abo";
      repo = "avy";
      type = "github";
    };
    avy-riben = {
      flake = false;
      owner = "akirak";
      repo = "emacs-dumb-japanese";
      type = "github";
    };
    beancount = {
      flake = false;
      owner = "beancount";
      repo = "beancount-mode";
      type = "github";
    };
    benchmark-init = {
      flake = false;
      owner = "akirak";
      repo = "benchmark-init-el";
      type = "github";
    };
    blamer = {
      flake = false;
      owner = "Artawower";
      repo = "blamer.el";
      type = "github";
    };
    buttercup = {
      flake = false;
      owner = "jorgenschaefer";
      repo = "emacs-buttercup";
      type = "github";
    };
    cape = {
      flake = false;
      owner = "minad";
      repo = "cape";
      type = "github";
    };
    citar = {
      flake = false;
      owner = "bdarcus";
      repo = "citar";
      type = "github";
    };
    citeproc = {
      flake = false;
      owner = "andras-simonyi";
      repo = "citeproc-el";
      type = "github";
    };
    closql = {
      flake = false;
      owner = "emacscollective";
      repo = "closql";
      type = "github";
    };
    color-theme-sanityinc-tomorrow = {
      flake = false;
      owner = "purcell";
      repo = "color-theme-sanityinc-tomorrow";
      type = "github";
    };
    consult = {
      flake = false;
      owner = "minad";
      repo = "consult";
      type = "github";
    };
    consult-dir = {
      flake = false;
      owner = "karthink";
      repo = "consult-dir";
      type = "github";
    };
    consult-org-dog = {
      flake = false;
      owner = "akirak";
      ref = "develop";
      repo = "org-dog";
      type = "github";
    };
    consult-project-extra = {
      flake = false;
      owner = "Qkessler";
      repo = "consult-project-extra";
      type = "github";
    };
    corfu = {
      flake = false;
      owner = "minad";
      repo = "corfu";
      type = "github";
    };
    dash = {
      flake = false;
      owner = "magnars";
      repo = "dash.el";
      type = "github";
    };
    deadgrep = {
      flake = false;
      owner = "Wilfred";
      repo = "deadgrep";
      type = "github";
    };
    dired-collapse = {
      flake = false;
      owner = "Fuco1";
      repo = "dired-hacks";
      type = "github";
    };
    dired-filter = {
      flake = false;
      owner = "Fuco1";
      repo = "dired-hacks";
      type = "github";
    };
    dired-hacks-utils = {
      flake = false;
      owner = "Fuco1";
      repo = "dired-hacks";
      type = "github";
    };
    dired-hide-dotfiles = {
      flake = false;
      owner = "mattiasb";
      repo = "dired-hide-dotfiles";
      type = "github";
    };
    dired-open = {
      flake = false;
      owner = "Fuco1";
      repo = "dired-hacks";
      type = "github";
    };
    doct = {
      flake = false;
      owner = "progfolio";
      repo = "doct";
      type = "github";
    };
    duckduckgo = {
      flake = false;
      owner = "akirak";
      repo = "duckduckgo.el";
      type = "github";
    };
    dumb-jump = {
      flake = false;
      owner = "jacktasia";
      repo = "dumb-jump";
      type = "github";
    };
    ediprolog = {
      flake = false;
      owner = "emacsmirror";
      repo = "ediprolog";
      type = "github";
    };
    editorconfig = {
      flake = false;
      owner = "editorconfig";
      repo = "editorconfig-emacs";
      type = "github";
    };
    eglot = {
      flake = false;
      owner = "joaotavora";
      repo = "eglot";
      type = "github";
    };
    el-patch = {
      flake = false;
      owner = "raxod502";
      repo = "el-patch";
      type = "github";
    };
    eldoc-eval = {
      flake = false;
      owner = "thierryvolpiatto";
      repo = "eldoc-eval";
      type = "github";
    };
    electric-operator = {
      flake = false;
      owner = "davidshepherd7";
      repo = "electric-operator";
      type = "github";
    };
    elisp-refs = {
      flake = false;
      owner = "Wilfred";
      repo = "elisp-refs";
      type = "github";
    };
    elx = {
      flake = false;
      owner = "emacscollective";
      repo = "elx";
      type = "github";
    };
    emacsql = {
      flake = false;
      owner = "skeeto";
      repo = "emacsql";
      type = "github";
    };
    emacsql-sqlite = {
      flake = false;
      owner = "skeeto";
      repo = "emacsql";
      type = "github";
    };
    embark = {
      flake = false;
      owner = "oantolin";
      repo = "embark";
      type = "github";
    };
    embark-consult = {
      flake = false;
      owner = "oantolin";
      repo = "embark";
      type = "github";
    };
    envrc = {
      flake = false;
      owner = "purcell";
      repo = "envrc";
      type = "github";
    };
    epkg = {
      flake = false;
      owner = "emacscollective";
      repo = "epkg";
      type = "github";
    };
    epkg-marginalia = {
      flake = false;
      owner = "emacscollective";
      repo = "epkg-marginalia";
      type = "github";
    };
    eros = {
      flake = false;
      owner = "xiongtx";
      repo = "eros";
      type = "github";
    };
    esup = {
      flake = false;
      owner = "jschaf";
      repo = "esup";
      type = "github";
    };
    esxml = {
      flake = false;
      owner = "tali713";
      repo = "esxml";
      type = "github";
    };
    evil-nerd-commenter = {
      flake = false;
      owner = "redguardtoo";
      repo = "evil-nerd-commenter";
      type = "github";
    };
    "f" = {
      flake = false;
      owner = "rejeep";
      repo = "f.el";
      type = "github";
    };
    fancy-dabbrev = {
      flake = false;
      owner = "jrosdahl";
      repo = "fancy-dabbrev";
      type = "github";
    };
    fanyi = {
      flake = false;
      owner = "condy0919";
      repo = "fanyi.el";
      type = "github";
    };
    forge = {
      flake = false;
      owner = "magit";
      repo = "forge";
      type = "github";
    };
    fullframe = {
      flake = false;
      type = "git";
      url = "https://git.sr.ht/~tomterl/fullframe";
    };
    fwb-cmds = {
      flake = false;
      owner = "tarsius";
      repo = "fwb-cmds";
      type = "github";
    };
    gcmh = {
      flake = false;
      type = "git";
      url = "https://gitlab.com/koral/gcmh.git";
    };
    ghelp = {
      flake = false;
      owner = "casouri";
      repo = "ghelp";
      type = "github";
    };
    ghelp-helpful = {
      flake = false;
      owner = "casouri";
      repo = "ghelp";
      type = "github";
    };
    ghub = {
      flake = false;
      owner = "magit";
      repo = "ghub";
      type = "github";
    };
    gif-screencast = {
      flake = false;
      type = "git";
      url = "https://gitlab.com/Ambrevar/emacs-gif-screencast.git";
    };
    git-attr = {
      flake = false;
      owner = "arnested";
      repo = "emacs-git-attr";
      type = "github";
    };
    git-auto-commit-mode = {
      flake = false;
      owner = "ryuslash";
      repo = "git-auto-commit-mode";
      type = "github";
    };
    git-commit = {
      flake = false;
      owner = "magit";
      repo = "magit";
      type = "github";
    };
    git-gutter = {
      flake = false;
      owner = "emacsorphanage";
      repo = "git-gutter";
      type = "github";
    };
    git-identity = {
      flake = false;
      owner = "akirak";
      repo = "git-identity.el";
      type = "github";
    };
    git-modes = {
      flake = false;
      owner = "magit";
      repo = "git-modes";
      type = "github";
    };
    github-linguist = {
      flake = false;
      owner = "akirak";
      repo = "github-linguist.el";
      type = "github";
    };
    go-translate = {
      flake = false;
      owner = "lorniu";
      repo = "go-translate";
      type = "github";
    };
    graphql = {
      flake = false;
      owner = "vermiculus";
      repo = "graphql.el";
      type = "github";
    };
    graphql-mode = {
      flake = false;
      owner = "davazp";
      repo = "graphql-mode";
      type = "github";
    };
    graphviz-dot-mode = {
      flake = false;
      owner = "ppareit";
      repo = "graphviz-dot-mode";
      type = "github";
    };
    haskell-mode = {
      flake = false;
      owner = "haskell";
      repo = "haskell-mode";
      type = "github";
    };
    hcl-mode = {
      flake = false;
      owner = "purcell";
      repo = "emacs-hcl-mode";
      type = "github";
    };
    helpful = {
      flake = false;
      owner = "Wilfred";
      repo = "helpful";
      type = "github";
    };
    hercules = {
      flake = false;
      owner = "wurosh";
      repo = "hercules";
      type = "github";
    };
    highlight-indent-guides = {
      flake = false;
      owner = "DarthFennec";
      repo = "highlight-indent-guides";
      type = "github";
    };
    hl-todo = {
      flake = false;
      owner = "tarsius";
      repo = "hl-todo";
      type = "github";
    };
    ht = {
      flake = false;
      owner = "Wilfred";
      repo = "ht.el";
      type = "github";
    };
    huan = {
      flake = false;
      owner = "akirak";
      repo = "huan.el";
      type = "github";
    };
    hydra = {
      flake = false;
      owner = "abo-abo";
      repo = "hydra";
      type = "github";
    };
    iedit = {
      flake = false;
      owner = "victorhge";
      repo = "iedit";
      type = "github";
    };
    inheritenv = {
      flake = false;
      owner = "purcell";
      repo = "inheritenv";
      type = "github";
    };
    ivy = {
      flake = false;
      owner = "abo-abo";
      repo = "swiper";
      type = "github";
    };
    json-mode = {
      flake = false;
      owner = "joshwnj";
      repo = "json-mode";
      type = "github";
    };
    json-snatcher = {
      flake = false;
      owner = "Sterlingg";
      repo = "json-snatcher";
      type = "github";
    };
    kind-icon = {
      flake = false;
      owner = "jdtsmith";
      repo = "kind-icon";
      type = "github";
    };
    kv = {
      flake = false;
      owner = "nicferrier";
      repo = "emacs-kv";
      type = "github";
    };
    leetcode = {
      flake = false;
      owner = "kaiwk";
      repo = "leetcode.el";
      type = "github";
    };
    license-templates = {
      flake = false;
      owner = "jcs-elpa";
      repo = "license-templates";
      type = "github";
    };
    link-hint = {
      flake = false;
      owner = "akirak";
      ref = "skip-invisible";
      repo = "link-hint.el";
      type = "github";
    };
    lispy = {
      flake = false;
      owner = "abo-abo";
      repo = "lispy";
      type = "github";
    };
    log4e = {
      flake = false;
      owner = "aki2o";
      repo = "log4e";
      type = "github";
    };
    lv = {
      flake = false;
      owner = "abo-abo";
      repo = "hydra";
      type = "github";
    };
    macrostep = {
      flake = false;
      owner = "joddie";
      repo = "macrostep";
      type = "github";
    };
    magit = {
      flake = false;
      owner = "magit";
      repo = "magit";
      type = "github";
    };
    magit-delta = {
      flake = false;
      owner = "dandavison";
      repo = "magit-delta";
      type = "github";
    };
    magit-section = {
      flake = false;
      owner = "magit";
      repo = "magit";
      type = "github";
    };
    magit-todos = {
      flake = false;
      owner = "alphapapa";
      repo = "magit-todos";
      type = "github";
    };
    marginalia = {
      flake = false;
      owner = "minad";
      repo = "marginalia";
      type = "github";
    };
    markdown-mode = {
      flake = false;
      owner = "jrblevin";
      repo = "markdown-mode";
      type = "github";
    };
    mini-modeline = {
      flake = false;
      owner = "kiennq";
      repo = "emacs-mini-modeline";
      type = "github";
    };
    move-dup = {
      flake = false;
      owner = "wyuenho";
      repo = "move-dup";
      type = "github";
    };
    nickel-mode = {
      flake = false;
      owner = "GTrunSec";
      repo = "nickel-mode";
      type = "github";
    };
    nix-mode = {
      flake = false;
      owner = "NixOS";
      repo = "nix-mode";
      type = "github";
    };
    nix26 = {
      flake = false;
      owner = "emacs-twist";
      ref = "develop";
      repo = "nix26.el";
      type = "github";
    };
    nov = {
      flake = false;
      type = "git";
      url = "https://depp.brause.cc/nov.el.git";
    };
    orderless = {
      flake = false;
      owner = "oantolin";
      repo = "orderless";
      type = "github";
    };
    org-autolist = {
      flake = false;
      owner = "calvinwyoung";
      repo = "org-autolist";
      type = "github";
    };
    org-bookmark-heading = {
      flake = false;
      owner = "alphapapa";
      repo = "org-bookmark-heading";
      type = "github";
    };
    org-dog = {
      flake = false;
      owner = "akirak";
      ref = "develop";
      repo = "org-dog";
      type = "github";
    };
    org-dog-embark = {
      flake = false;
      owner = "akirak";
      ref = "develop";
      repo = "org-dog";
      type = "github";
    };
    org-dog-facade = {
      flake = false;
      owner = "akirak";
      ref = "develop";
      repo = "org-dog";
      type = "github";
    };
    org-make-toc = {
      flake = false;
      owner = "alphapapa";
      repo = "org-make-toc";
      type = "github";
    };
    org-ml = {
      flake = false;
      owner = "ndwarshuis";
      repo = "org-ml";
      type = "github";
    };
    org-noter = {
      flake = false;
      owner = "weirdNox";
      repo = "org-noter";
      type = "github";
    };
    org-ql = {
      flake = false;
      owner = "alphapapa";
      repo = "org-ql";
      type = "github";
    };
    org-recur = {
      flake = false;
      owner = "m-cat";
      repo = "org-recur";
      type = "github";
    };
    org-remark = {
      flake = false;
      owner = "nobiot";
      repo = "org-remark";
      type = "github";
    };
    org-reverse-datetree = {
      flake = false;
      owner = "akirak";
      repo = "org-reverse-datetree";
      type = "github";
    };
    org-starter = {
      flake = false;
      owner = "akirak";
      repo = "org-starter";
      type = "github";
    };
    org-super-agenda = {
      flake = false;
      owner = "alphapapa";
      repo = "org-super-agenda";
      type = "github";
    };
    org-transclusion = {
      flake = false;
      owner = "nobiot";
      repo = "org-transclusion";
      type = "github";
    };
    org-volume = {
      flake = false;
      owner = "akirak";
      repo = "org-volume";
      type = "github";
    };
    org-web-tools = {
      flake = false;
      owner = "alphapapa";
      repo = "org-web-tools";
      type = "github";
    };
    orgit = {
      flake = false;
      owner = "magit";
      repo = "orgit";
      type = "github";
    };
    orglink = {
      flake = false;
      owner = "tarsius";
      repo = "orglink";
      type = "github";
    };
    ov = {
      flake = false;
      owner = "emacsorphanage";
      repo = "ov";
      type = "github";
    };
    package-lint = {
      flake = false;
      owner = "purcell";
      repo = "package-lint";
      type = "github";
    };
    page-break-lines = {
      flake = false;
      owner = "purcell";
      repo = "page-break-lines";
      type = "github";
    };
    parsebib = {
      flake = false;
      owner = "joostkremers";
      repo = "parsebib";
      type = "github";
    };
    pcre2el = {
      flake = false;
      owner = "joddie";
      repo = "pcre2el";
      type = "github";
    };
    pocket-lib = {
      flake = false;
      owner = "alphapapa";
      repo = "pocket-lib.el";
      type = "github";
    };
    pocket-reader = {
      flake = false;
      owner = "alphapapa";
      repo = "pocket-reader.el";
      type = "github";
    };
    poet-theme = {
      flake = false;
      owner = "kunalb";
      repo = "poet";
      type = "github";
    };
    popper = {
      flake = false;
      owner = "karthink";
      repo = "popper";
      type = "github";
    };
    popup = {
      flake = false;
      owner = "auto-complete";
      repo = "popup-el";
      type = "github";
    };
    posframe = {
      flake = false;
      owner = "tumashu";
      repo = "posframe";
      type = "github";
    };
    project-hercules = {
      flake = false;
      owner = "akirak";
      repo = "project-hercules.el";
      type = "github";
    };
    promise = {
      flake = false;
      owner = "chuntaro";
      repo = "emacs-promise";
      type = "github";
    };
    puni = {
      flake = false;
      owner = "AmaiKinono";
      repo = "puni";
      type = "github";
    };
    queue = {
      flake = false;
      owner = "dmgerman";
      repo = "predictive";
      type = "github";
    };
    rainbow-delimiters = {
      flake = false;
      owner = "Fanael";
      repo = "rainbow-delimiters";
      type = "github";
    };
    rainbow-identifiers = {
      flake = false;
      owner = "Fanael";
      repo = "rainbow-identifiers";
      type = "github";
    };
    rainbow-mode = {
      flake = false;
      owner = "emacsmirror";
      repo = "rainbow-mode";
      type = "github";
    };
    reformatter = {
      flake = false;
      owner = "purcell";
      repo = "emacs-reformatter";
      type = "github";
    };
    repl-toggle = {
      flake = false;
      type = "git";
      url = "https://git.sr.ht/~tomterl/repl-toggle";
    };
    request = {
      flake = false;
      owner = "tkf";
      repo = "emacs-request";
      type = "github";
    };
    restclient = {
      flake = false;
      owner = "pashky";
      repo = "restclient.el";
      type = "github";
    };
    riben = {
      flake = false;
      owner = "akirak";
      repo = "emacs-dumb-japanese";
      type = "github";
    };
    rich-minority = {
      flake = false;
      owner = "Malabarba";
      repo = "rich-minority";
      type = "github";
    };
    "s" = {
      flake = false;
      owner = "magnars";
      repo = "s.el";
      type = "github";
    };
    setup = {
      flake = false;
      type = "git";
      url = "https://git.sr.ht/~pkal/setup";
    };
    string-inflection = {
      flake = false;
      owner = "akicho8";
      repo = "string-inflection";
      type = "github";
    };
    super-save = {
      flake = false;
      owner = "bbatsov";
      repo = "super-save";
      type = "github";
    };
    svelte-mode = {
      flake = false;
      owner = "leafOfTree";
      repo = "svelte-mode";
      type = "github";
    };
    svg-lib = {
      flake = false;
      owner = "rougier";
      repo = "svg-lib";
      type = "github";
    };
    swiper = {
      flake = false;
      owner = "abo-abo";
      repo = "swiper";
      type = "github";
    };
    symbol-overlay = {
      flake = false;
      owner = "wolray";
      repo = "symbol-overlay";
      type = "github";
    };
    tagedit = {
      flake = false;
      owner = "magnars";
      repo = "tagedit";
      type = "github";
    };
    taxy = {
      flake = false;
      owner = "alphapapa";
      repo = "taxy.el";
      type = "github";
    };
    taxy-magit-section = {
      flake = false;
      owner = "alphapapa";
      ref = "package/taxy-magit-section";
      repo = "taxy.el";
      type = "github";
    };
    titlecase = {
      flake = false;
      owner = "duckwork";
      repo = "titlecase.el";
      type = "github";
    };
    tree-sitter = {
      flake = false;
      owner = "emacs-tree-sitter";
      ref = "0.18.0";
      repo = "elisp-tree-sitter";
      type = "github";
    };
    tree-sitter-langs = {
      flake = false;
      owner = "emacs-tree-sitter";
      ref = "0.11.6";
      repo = "tree-sitter-langs";
      type = "github";
    };
    treepy = {
      flake = false;
      owner = "volrath";
      repo = "treepy.el";
      type = "github";
    };
    ts = {
      flake = false;
      owner = "alphapapa";
      repo = "ts.el";
      type = "github";
    };
    tsc = {
      flake = false;
      owner = "emacs-tree-sitter";
      ref = "0.18.0";
      repo = "elisp-tree-sitter";
      type = "github";
    };
    turbo-log = {
      flake = false;
      owner = "Artawower";
      repo = "turbo-log";
      type = "github";
    };
    twist = {
      flake = false;
      owner = "emacs-twist";
      ref = "develop";
      repo = "twist.el";
      type = "github";
    };
    typescript-mode = {
      flake = false;
      owner = "emacs-typescript";
      repo = "typescript.el";
      type = "github";
    };
    vertico = {
      flake = false;
      owner = "minad";
      repo = "vertico";
      type = "github";
    };
    visual-fill-column = {
      flake = false;
      ref = "main";
      type = "git";
      url = "https://codeberg.org/joostkremers/visual-fill-column.git";
    };
    vterm = {
      flake = false;
      owner = "akermu";
      repo = "emacs-libvterm";
      type = "github";
    };
    which-key = {
      flake = false;
      owner = "justbur";
      repo = "emacs-which-key";
      type = "github";
    };
    windswap = {
      flake = false;
      owner = "purcell";
      repo = "windswap";
      type = "github";
    };
    with-editor = {
      flake = false;
      owner = "magit";
      repo = "with-editor";
      type = "github";
    };
    ws-butler = {
      flake = false;
      owner = "lewang";
      repo = "ws-butler";
      type = "github";
    };
    xterm-color = {
      flake = false;
      owner = "atomontage";
      repo = "xterm-color";
      type = "github";
    };
    yaml = {
      flake = false;
      owner = "zkry";
      repo = "yaml.el";
      type = "github";
    };
    yaml-mode = {
      flake = false;
      owner = "yoshiki";
      repo = "yaml-mode";
      type = "github";
    };
    yasnippet = {
      flake = false;
      owner = "joaotavora";
      repo = "yasnippet";
      type = "github";
    };
    zoutline = {
      flake = false;
      owner = "abo-abo";
      repo = "zoutline";
      type = "github";
    };
  };
  outputs = {...}: {};
}
